unit PlotDemo;
// Fred Harthoorn, f.harthoorn@ziggo.nl

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Db, DBTables, Grids, DBGrids, StdCtrls, Math, Mask,
  ComCtrls, Spin;

const LastPlotIndex = 99 ;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    ButtonPlot: TButton;
    Label1: TLabel;
    ButtonMove: TButton;
    ButtonClear: TButton;
    Memo1: TMemo;
    Area: TImage;
    SpinEdit1: TSpinEdit;
    Label2: TLabel;
    Label3: TLabel;
    plotlamp: TImage;
    movelamp: TImage;
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ButtonPlotClick(Sender: TObject);
    procedure AreaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ButtonClearClick(Sender: TObject);
    procedure ButtonMoveClick(Sender: TObject);
    procedure AreaMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AreaMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure SpinEdit1Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    plotsession, verplaats,
    verplaatsenable : boolean ;
    procedure Clear ;
    procedure Plot(x, y, size : integer ; Color : TColor) ;
    procedure Toonplotpunten ;
    procedure PlotInterpol;
    procedure Interpoleer;
    procedure PlotText(x, y : Integer ; Txt : String);
    procedure ZoekPlot(x, y: integer ; var theindex : integer) ;
  end;

var
  Form1: TForm1;
  startx, starty,
  ThePlotIndex,
  plotindex,
  gradatie : integer ;
  Plotrij: array[0..LastPlotIndex,0..1] of integer ;
  (*----------------Hier start PSpline.dll ---------------*)
const
  Copyright = 'Copyright 2002, F.Harthoorn, f.harthoorn@ziggo.nl' ; // Granted to Blender.org
type
  TPlotlist = array[1..128,0..1] of integer ;

var
  X0, X1, X2, X3,
  D3X, D2X, D1X, X : integer ;
  Y0, Y1, Y2, Y3,
  D3Y, D2Y, D1Y, Y : integer ;
  Pstartx,  Pstarty,
  Pgradatie,
  SplinePts : integer ;
  Plotlist  : TPlotlist ;
  
  procedure InitSpline(StartwX, StartwY: integer; var Gr : Integer);
  procedure MakeDDDt(v, w: Integer) ;
  procedure Interpolate(px, py : integer);
  procedure GetValue(i : integer ; var x, y : integer);




implementation

procedure InitSpline(StartwX, StartwY: integer; var Gr : Integer) ;
begin
  SplinePts := 1 ;
  Pgradatie := 0 ;
  if gr <=0 then gr := 2;
  if gr >= 128 then gr := 128;
  while Gr > 1 do
  begin
    SplinePts := SplinePts * 2 ;
    inc(PGradatie) ;
    gr := gr shr 1 ;
  end ;
  Gr := SplinePts ;
  Pstartx := startwX ;
  Pstarty := startwY ;
  X0 := 0;   X1  := X0; X2 := X0; X3 := X0;
  D3X := 0; D2X := 0;  D1X := 0;  X := X0;
  Y0 := 0;   Y1  := Y0; Y2 := Y0; Y3 := Y0;
  D3Y := 0; D2Y := 0;  D1Y := 0;  Y := Y0;
end ;

Procedure makedddt(v, w : Integer) ;
begin
//  X0 := X1; X1 := X2; X2 := X3; X3 := (v -Pstartx) ;
//  DDDX := -X0+X1+X1+X1-X2-X2-X2+X3;
//  Y0 := Y1; Y1 := Y2; Y2 := Y3; Y3 := (w -PstartY) ;
//  DDDY := -Y0+Y1+Y1+Y1-Y2-Y2-Y2+Y3;
  (* in assembler: *)
  asm
    CLD
    MOV ECX, 3
    PUSH ESI
    PUSH EDI
    LEA ESI, X1
    LEA EDI, X0
    REP MOVSD
    MOV EAX, V
    SUB EAX, PStartX
    MOV X3, EAX
    SUB EAX, X0
    MOV ECX, X1
    SUB ECX, X2
    ADD EAX, ECX
    SHL ECX, 1
    ADD EAX, ECX
    MOV D3X, EAX
    CLD
    MOV ECX, 3
    LEA ESI, Y1
    LEA EDI, Y0
    REP MOVSD
    MOV EAX, w
    SUB EAX, PStartY
    MOV Y3, EAX
    SUB EAX, Y0
    MOV ECX, Y1
    SUB ECX, Y2
    ADD EAX, ECX
    SHL ECX, 1
    ADD EAX, ECX
    MOV D3Y, EAX
    POP EDI
    POP ESI
  end ;
end ;

procedure Interpolate(px, py: integer);
var i : integer ;
    Xtmp, YTmp,
    ShiftLen : integer ;
begin
   ShiftLen := 3 * PGradatie ;
   makedddt(px, py) ;
   for i := 1 to SplinePts do
   begin
//     D2X := D2X + d3x ; D1X := D1X  + D2X; X := X + D1X  ;
//     D2Y := D2Y + D3Y ; D1Y := D1Y  + D2Y; Y := Y + D1Y  ;
//     XTmp := X div ( 1 shl ShiftLen) ;
//     YTmp := Y div ( 1 shl ShiftLen) ;
    (* in assembler *)
    asm
      MOV EAX, D3X
      ADD EAX, D2X
      MOV D2X, EAX
      ADD EAX, D1X
      MOV D1X, EAX
      ADD EAX, X
      MOV X,   EAX
      Mov ECX, ShiftLen
      SAR EAX, CL
      MOV XTmp,EAX

      MOV EAX, D3Y
      ADD EAX, D2Y
      MOV D2Y, EAX
      ADD EAX, D1Y
      MOV D1Y, EAX
      ADD EAX, Y
      MOV Y,   EAX
      Mov ECX, ShiftLen
      SAR EAX, CL
      MOV YTmp,EAX
    end ;
     Plotlist[i,0] := Pstartx + XTmp;
     Plotlist[i,1] := Pstarty + YTmp;
  end
end ;

procedure GetValue(i : integer;var  x, y : integer) ;
begin
  if i < 1 then i := 1 ;
  if i > SplinePts then i := SplinePts ;
  X := Plotlist[i, 0]  ;
  Y := Plotlist[i, 1] ;
end ;
(*
exports
    InitSpline,
    Interpolate,
    GetValue;
end.
*)
  (*----------------Hier eindigt Pspline.dll ------------------*)
  (* De volgende procedures in PSpline.dll zijn beschikbaar:

     InitSpline(StartwX, StartwY:integer ; var Gradatie : Integer);

     StartwaardeX : Van het eerste punt de X-coordinaat
                    in dit programma plotrij[0,0]
     StartwaardeY : Van het eerste punt de Y-coordinaat
                    in dit programma plotrij[0,1]
     Gradatie:      het verlangde aantal interpolatiesecties
                    tussen twee opeenvolgende punten.
                    Het getal wordt afgerond op een macht van
                    2. Het aantal interpolatiepunten is:
                    2^Gradatie

     Interpolate(px, py : integer);

     Interpolate zorgt voor de volgende rij van
     interpolatiepunten.
     px            : De x-coordinaat van het volgende punt.
     py            : De y-coordinaat van het volgende punt.
                     In dit programma:
                     px = plotrij[i,0] (i = 1..99)
                     py = plotrij[i,1] (i = 1..99)

     GetValue(i: integer; var x, y: integer);

     Nadat een nieuw punt is opgegeven, kun je
     hiermee de volgende serie interpolatie punten ophalen.
     i              : Het ide interpolatiepunt in de rij
                      van interpolatiepunten (i = 1..Gradatie)
     x              : De x-coordinaat van interpolatiepunt i.
     y              : De y-coordinaat van interpolatiepunt i.

     De procedures in PSpline.dll declareer je zoals aangegeven
     bij de volgende drie procedures( voor de paramaters mag je
     zelf namen kiezen, bijv: StartwX had je ook gewoon x kunnen
     noemen.):

  procedure InitSpline(StartwX,
            StartwY:integer ; var Gradatie : Integer); external 'PSpline.dll';
  procedure Interpolate(px, py : integer)            ; external 'PSpline.dll';
  procedure GetValue(i: integer; var x, y: integer)  ; external 'PSpline.dll';

   *)
(*implementation*)

{$R *.DFM}

(*----------------------------------------------------------*)
(*                  Initialisatie-routines                  *)
(*----------------------------------------------------------*)

(* Maak het tekengebied schoon: *)
procedure TForm1.Clear ;
begin
  with Area.Canvas do
  begin
    Brush.color := clWhite ;
    Pen.Color := clBlack ;
    Rectangle(0,0,width,height) ;
//    invalidate
  end ;
end ;

(* Creeer het formulier: *)
procedure TForm1.FormCreate(Sender: TObject);
begin
  Clear ;
end;

(* Initialiseer het formulier: *)
procedure TForm1.FormShow(Sender: TObject);
begin
  With Form1 do
  begin
    Plotsession := true ; plotlamp.visible:= true ;
    verplaats := false ;  
    verplaatsenable := false ; movelamp.visible := false ;
    plotindex := 0 ;
    SpinEdit1.value := 4 ;
    Gradatie := 1 shl SPinEdit1.value ;
    label2.caption :=
       'Aantal'+ #13#10 + 'interpolatiepunten' +
        #13#10 + 'als macht van 2:';
  end
end;

(*----------------------------------------------------------*)
(*                    Grafische routines                    *)
(*----------------------------------------------------------*)

(* Afstandbepaling *)
function sqrdistance( x0,y0,x1,y1 : integer) : integer ;
begin
  sqrdistance := sqr(x1 - x0)+ sqr(y1 - y0)
end ;

(* Om een plotpunt te verschuiven moet eerst bepaald worden
   welk plotpunt het dichtst bij de cursor ligt: *)
procedure TForm1.ZoekPlot(x, y: integer ; var theindex : integer) ;
var i,
    tdistance,
    distance : integer ;

begin
   y := (Area.Height - Y );
   i := 0 ;
   (* Bepaal het kwadraat van de afstand : sqr(x) + sqr(y)  tussen
      de cursorpositie en een plotpunt. Die met kleinste afstand
      tot de cursorpositie is het gezochte plotpunt *)
   distance := sqrdistance(plotrij[0,0], plotrij[0,1], x, y) ;
   theindex := 0 ;
   while i < plotindex do
   begin
     inc(i);
     tdistance :=  sqrdistance(plotrij[i,0], plotrij[i,1], x, y) ;
     if tdistance < distance then
     begin
       distance := tdistance ;
       theindex := i ;
     end
   end ;
   if distance > 800 then theindex := -1
end ;

(* Plot een tekst op het tekengebied *)
procedure TForm1.PlotText(x, y : Integer ; Txt : String);
begin
  Area.Canvas.TextOut(x, Area.Height - y, Txt)
end ;

(* Plot een punt op (x,y) met opgegeven grootte en kleur: *)
procedure TForm1.Plot(x, y, size : integer ; Color : TColor) ;
var i, j,
    l     : integer ;
begin
  size := size -1 ;
  l := -size ;
  if Size = 1 then
  begin
    l := 0
  end ;
  for i := l to size do
  for j := l to size do
  Area.Canvas.Pixels[x+i, Area.Height-y-j] := Color ;
end ;

procedure TForm1.Toonplotpunten ;
var i : integer ;
begin
  for i := 0 to Plotindex -1 do
  begin
    Plot(plotrij[i,0],plotrij[i,1], 4, clRed);
    PlotText(plotrij[i,0] +4,
                    plotrij[i,1] -2,IntToStr(i)) ;
  end ;

end ;

(* Haal de interpolatie punten op en plot ze *)
procedure TForm1.PlotInterpol;
var i,
    x, y : integer ;
begin
  for i := 0 to gradatie do
  begin
    GetValue(i, x, y) ;
    plot(x, y, 2, clGreen)
  end ;
end ;

(* Initialiseer de interpolatie routine
   en plot de interpolatie punten:   *)
procedure TForm1.Interpoleer ;
var i : integer ;
begin
  verplaatsenable := false ;
  initspline(plotrij[0,0], plotrij[0,1], gradatie) ;
  for i := 0 to plotindex - 1 do
  begin
    Interpolate(plotrij[i,0], plotrij[i,1]) ;
    PlotInterpol;
  end ;
  (* trucje: herhaal het laatste plotpunt om tot aan het laatste punt
     te interpoleren *)
  Interpolate(plotrij[plotindex-1,0], plotrij[plotindex-1,1]) ;
  PlotInterpol;
end;

(* Verwijder alle punten op het tekengebied *)
procedure TForm1.ButtonClearClick(Sender: TObject);
begin
  plotsession := true ; plotlamp.visible:= true ;
  verplaats := false ;   movelamp.visible := false ;
  verplaatsenable := false ;
  Clear ;
  PlotIndex := 0 ;
end;

(* Enable het verplaatsen *)
procedure TForm1.ButtonMoveClick(Sender: TObject);
begin
  plotsession := false ; plotlamp.visible:= false ;
  verplaats := true;     movelamp.visible := true;
  Memo1.lines[0] := 'Je kunt een punt aanklikken en met mouse-down verslepen.' ;
end;

(* Bepaal welk plotpunt verschoven moet worden: *)
procedure TForm1.AreaMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var theindex : integer ;
begin
  If verplaats and (not verplaatsenable) then
  begin
    zoekplot(X, Y, TheIndex) ;
    if TheIndex <> -1 then
    begin
      ThePlotIndex := TheIndex ;
      Verplaatsenable := true ;
      (* Dit is tricky, als de mouse verschoven wordt moet hij
         ook nog down zijn om te verschuiven. *)
    end
  end
end;

(*----------------------------------------------------------*)
(*                 Mouse en andere events:                  *)
(*----------------------------------------------------------*)


(* Enable het  invoeren van plotpunten: *)
procedure TForm1.ButtonPlotClick(Sender: TObject);
begin
  Plotsession := true ; plotlamp.visible:= true ;
  verplaats := false ;  movelamp.visible := false ;
  Memo1.Lines[0] :=
  'Je kunt nu binnen het areaal clicken om plotpunten  te maken(het maximum=100).';
end;

(* Vastleggen van een plotpunt in de plotrij: *)
procedure TForm1.AreaMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var i : integer ;
    tmpcolor : TColor ;
    tmpSize  : integer ;
    Time0 : TDateTime ;

begin
  (* Een plotpunt opslaan *)
  Clear ;
  if plotsession then
  begin
    if plotindex < LastPlotIndex then
    begin
      plotrij[plotindex,0] := X ;
      plotrij[plotindex,1] :=  (Area.Height - Y);
      inc(plotindex);
    end
    else
    with Memo1 do
    begin
      with font do
      begin
        tmpColor := color ;
        tmpSize :=  size ;
        color := clRed ;
        size  := 12 ;
        lines[0] := 'Het maximum aantal plotpunten is gezet op 100' ;
        Time0 := Now + 0.00004 ;
        while Time0 > now do ;
        color := tmpColor ;
        size := tmpSize ;
      end
    end
  end;
  (* En de coordinaten opnieuw opslaan als het plotpunet verplaats wordt: *)
  if verplaats then Verplaatsenable := false ;
    (* Einde van het verschuiven, want de muis is losgelaten *)
  for i := 0 to Plotindex -1 do
  begin
    Plot(plotrij[i,0],plotrij[i,1], 4, clRed);
    PlotText(plotrij[i,0] +4,
                    plotrij[i,1] -2,IntToStr(i)) ;
  end ;
  Interpoleer;
end;



(* Verschuif een plotpunt: *)
procedure TForm1.AreaMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if verplaats and verplaatsenable then
  begin
    (* Plot het punt op de plaats zoals opgenomen in de plotij
        in  de achtergrondkleur om hem op te heffen *)
    Plot(plotrij[ThePlotIndex,0],plotrij[ThePlotindex,1], 4, clWhite);
    (* Doe het zelfde met het bij behorende getal *)
    Area.canvas.font.Color := clWhite ;
    PlotText(plotrij[ThePlotindex,0] +4,
                    plotrij[ThePlotindex,1] -2,IntToStr(ThePlotindex)) ;
    (* Vervang de positie van het plotpunt door de positie van de mouse *)
    Area.canvas.font.Color := clBlack ;
    plotrij[ThePlotindex,0] := X ;
    plotrij[ThePlotindex,1] := Area.Height - Y ;
    (* En plot het punt op de nieuwe positie *)
    Plot(plotrij[ThePlotindex,0],plotrij[ThePlotindex,1], 4, clRed);
    PlotText(plotrij[ThePlotindex,0]+4,
                    plotrij[ThePlotindex,1]-2,IntToStr(ThePlotindex)) ;
  end ;
end;

(* Bepaal de gradatie uit het spinwiel op het formulier: *)
procedure TForm1.SpinEdit1Change(Sender: TObject);
begin
  gradatie := 1 shl Spinedit1.value ;
  label3.caption :=
  IntToStr(gradatie) + '  interpolatiepunten.';
  Clear ;
  Interpoleer ;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  ToonPlotpunten
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Clear ;
  Interpoleer
end;

end.


