unit PSpline;

// Fred Harthoorn, f.harthoorn@ziggo.nl
// Dit is een gedempte spline

interface

{$DEFINE UseAssembler}
{$Define TwoDimensions}
//{$Define MoreDimensions}

const
{$IFDEF TwoDimensions}
  Dim = 2;
{$ENDIF}

{$IFDEF MoreDimensions}
  Dim = 3;  // Kan ook hoger zijn voor abstracte toepassingen.
{$ENDIF}

type
  TPlotlist = array[1..dim,1..128] of integer ;

var
{$IFDEF Twodimensions}
  X0, X1, X2, X3,
  D3X, D2X, D1X, X,
  Pstartx            : integer ;
  Y0, Y1, Y2, Y3,
  D3Y, D2Y, D1Y, Y,
  Pstarty            : integer ;
{$ENDIF}

{$IFDEF MoreDimensions}
  series : array[1..dim,0..3] of integer ;
  deriv  : array[1..dim,0..3] of integer ;
  Pstart : array[1..dim] of integer ;
{$ENDIF}

  Pgradatie,
  SplinePts : integer ;
  Plotlist  : TPlotlist ;

  function  InfoWideString : WideString ;

{$IFDEF TwoDimensions}
  procedure InitSpline(StartwX, StartwY: integer; var Gr : Integer);
  Procedure makedddt(v, w : Integer) ;
  procedure Interpolate(px, py : integer);
  procedure GetValue(i : integer ; var x, y : integer);
{$ENDIF}

{$IFDEF MoreDimensions}
  procedure InitSpline(DimI, StartW: integer; var Gr : Integer) ;
  procedure makedddt(var StartValue, X0, X1, X2, X3, D3X : integer; v: Integer) ;
  procedure Interpolate(DimI, P: integer);
  procedure GetValue(DimI, i : integer; var X : integer) ;
{$ENDIF}


implementation

function InfoWideString: WideString ;
begin
  Result := 'Copyright 2004, F.Harthoorn, f.harthoorn@ziggo.nl'; // Granted to Blender.org
end ;  

{$IFDEF MoreDimensions}
procedure InitSpline(DimI, StartW: integer; var Gr : Integer) ;
var i,j : integer ;
begin
  SplinePts := 1 ;
  Pgradatie := 0 ;
  if gr <=0 then gr := 2;
  if gr >= 128 then gr := 128;
  while Gr > 1 do
  begin
    SplinePts := SplinePts * 2 ;
    inc(PGradatie) ;
    gr := gr shr 1 ;
  end ;
  Gr := SplinePts ;
  Pstart[DimI] := startW ;
  for i := 1 to dim do
  for j := 0 to 3 do
  begin
    series[i,j] := 0 ;
    deriv[i,j] := 0 ;
  end ;
end ;
{$ENDIF}

{$IFDEF TwoDimensions}
procedure InitSpline(StartwX, StartwY: integer; var Gr : Integer) ;
begin
  SplinePts := 1 ;
  Pgradatie := 0 ;
  if gr <=0 then gr := 2;
  if gr >= 128 then gr := 128;
  while Gr > 1 do
  begin
    SplinePts := SplinePts * 2 ;
    inc(PGradatie) ;
    gr := gr shr 1 ;
  end ;
  Gr := SplinePts ;
  Pstartx := startwX ;
  Pstarty := startwY ;
  X0 := 0;   X1  := X0; X2 := X0; X3 := X0;
  D3X := 0; D2X := 0;  D1X := 0;  X := X0;
  Y0 := 0;   Y1  := Y0; Y2 := Y0; Y3 := Y0;
  D3Y := 0; D2Y := 0;  D1Y := 0;  Y := Y0;
end ;
{$ENDIF}

{$IFDEF MoreDimensions}
Procedure makedddt(var StartValue, X0, X1, X2, X3, D3X : integer; V: Integer) ;
begin
{$IFDEF UseAssembler}
  asm
    CLD
    MOV ECX, 3
    PUSH ESI
    PUSH EDI
    LEA ESI, X1
    LEA EDI, X0
    REP MOVSD
    MOV EAX, V
    SUB EAX, StartValue
    MOV X3, EAX
    SUB EAX, X0
    MOV ECX, X1
    SUB ECX, X2
    ADD EAX, ECX
    SHL ECX, 1
    ADD EAX, ECX
    MOV D3X, EAX
    POP EDI
    POP ESI
  end ;
{$ELSE}
   X0 := X1; X1 := X2; X2 := X3; X3 := (V -Startvalue) ;
  D3X := -X0+X1+X1+X1-X2-X2-X2+X3;
{$ENDIF}
end ;
{$ENDIF}

{$IFDEF Twodimensions}
Procedure makedddt(v, w : Integer) ;
begin
{$IFDEF UseAssembler}
  asm
    CLD
    MOV ECX, 3
    PUSH ESI
    PUSH EDI
    LEA ESI, X1
    LEA EDI, X0
    REP MOVSD
    MOV EAX, V
    SUB EAX, PStartX
    MOV X3, EAX
    SUB EAX, X0
    MOV ECX, X1
    SUB ECX, X2
    ADD EAX, ECX
    SHL ECX, 1
    ADD EAX, ECX
    MOV D3X, EAX
    CLD
    MOV ECX, 3
    LEA ESI, Y1
    LEA EDI, Y0
    REP MOVSD
    MOV EAX, w
    SUB EAX, PStartY
    MOV Y3, EAX
    SUB EAX, Y0
    MOV ECX, Y1
    SUB ECX, Y2
    ADD EAX, ECX
    SHL ECX, 1
    ADD EAX, ECX
    MOV D3Y, EAX
    POP EDI
    POP ESI
  end ;
{$ELSE}
  X0 := X1; X1 := X2; X2 := X3; X3 := (v -Pstartx) ;
  D3X := -X0+X1+X1+X1-X2-X2-X2+X3;
  Y0 := Y1; Y1 := Y2; Y2 := Y3; Y3 := (w -PstartY) ;
  D3Y := -Y0+Y1+Y1+Y1-Y2-Y2-Y2+Y3;
{$ENDIF}
end ;
{$ENDIF}

{$IFDEF MoreDimensions}
procedure Interpolate(DimI, P: integer);
var i : integer ;
    Ptmp,
    ShiftLen : integer ;
begin
   ShiftLen := 3 * PGradatie ;
   makedddt( PStart[DimI], series[DimI,0],series[DimI,1], series[DimI,2],
                   series[DimI,3], deriv[DimI,3], P) ;
   for i := 1 to SplinePts do
   begin
     deriv[DimI,2] := deriv[DimI,2] + deriv[DimI,3];
     deriv[DimI,1] := deriv[DimI,1] + deriv[DimI,2];
     deriv[DimI,0] := deriv[DimI,0] + deriv[DimI,1];
     PTmp := deriv[DimI,0] div ( 1 shl ShiftLen) ;
     Plotlist[DimI,i] := Pstart[DimI] + PTmp;
  end
end ;
{$ENDIF}

{$IFDEF TwoDimensions}
procedure Interpolate(px, py: integer);
var i : integer ;
    Xtmp, YTmp,
    ShiftLen : integer ;
begin
   ShiftLen := 3 * PGradatie ;
   makedddt(px, py) ;
   for i := 1 to SplinePts do
   begin
{$IFDEF UseAssembler}
    asm
      MOV EAX, D3X
      ADD EAX, D2X
      MOV D2X, EAX
      ADD EAX, D1X
      MOV D1X, EAX
      ADD EAX, X
      MOV X,   EAX
      Mov ECX, ShiftLen
      SAR EAX, CL
      MOV XTmp,EAX

      MOV EAX, D3Y
      ADD EAX, D2Y
      MOV D2Y, EAX
      ADD EAX, D1Y
      MOV D1Y, EAX
      ADD EAX, Y
      MOV Y,   EAX
      Mov ECX, ShiftLen
      SAR EAX, CL
      MOV YTmp,EAX
    end ;
{$ELSE}
     D2X := D2X + d3x ; D1X := D1X  + D2X; X := X + D1X  ;
     D2Y := D2Y + D3Y ; D1Y := D1Y  + D2Y; Y := Y + D1Y  ;
     XTmp := X shr ShiftLen  ;
     YTmp := Y shr ShiftLen  ;
{$ENDIF}
     Plotlist[1,i] := Pstartx + XTmp;
     Plotlist[2,i] := Pstarty + YTmp;
  end
end ;
{$ENDIF}

{$IFDEF MoreDimensions}
procedure GetValue(DimI, i : integer; var X : integer) ;
begin
  if i < 1 then i := 1 ;
  if i > SplinePts then i := SplinePts ;
  X := Plotlist[DimI, i]  ;

end ;
{$ENDIF}

{$IFDEF TwoDimensions}
procedure GetValue(i : integer;var  x, y : integer) ;
begin
  if i < 1 then i := 1 ;
  if i > SplinePts then i := SplinePts ;
  X := Plotlist[1,i]  ;
  Y := Plotlist[2,i] ;
end ;
{$ENDIF}


exports
    InfoWideString,
    InitSpline,
    Makedddt,
    Interpolate,
    GetValue;
end.




