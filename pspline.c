/**
 * Original code by 'Fred Harthoorn' ported to C by 'Campbell Barton'
 *
 * Build:
 *    gcc pspline.c -Wall -o pspline
 */

#include <stdio.h>

#define DIMS 3
#define SPLINE_TESS_MAX 128

struct FredCurve {
	int series[DIMS][4];
	int deriv[DIMS][4];
	int PStart[DIMS];

	int PGradatie;
	int SplinePts;
	int Plotlist[SPLINE_TESS_MAX][3];
};

static void InitSpline(struct FredCurve *fc, const int DimI, const int StartW, int gr)
{
	int i, j;
	fc->SplinePts = 1;
	fc->PGradatie = 0;
	if      (gr <= 0)                gr = 2;
	else if (gr >= SPLINE_TESS_MAX)  gr = SPLINE_TESS_MAX;

	while (gr > 1) {
		fc->SplinePts *= 2;
		fc->PGradatie += 1;
		gr = gr >> 1;
	}
	fc->PStart[DimI] = StartW;
	for (i = 0; i < DIMS; i++) {
		for (j = 0; j < 4; j++) {
			fc->series[i][j] = 0;
			fc->deriv[i][j]  = 0;
		}
	}
}

#define MAKEDDDT(StartValue, X0, X1, X2, X3, D3X, V) \
{ \
	X0 = X1; X1 = X2; X2 = X3; X3 = (V - StartValue); \
	D3X = -X0 + X1 + X1 + X1 - X2 - X2 - X2 + X3; \
} (void)0

static void Interpolate(struct FredCurve *fc, const int DimI, const int P)
{
	int i, PTmp, ShiftLen;

	ShiftLen = 3 * fc->PGradatie;
	MAKEDDDT(fc->PStart[DimI],
	         fc->series[DimI][0],
	         fc->series[DimI][1],
	         fc->series[DimI][2],
	         fc->series[DimI][3],
	         fc->deriv[DimI][3], P);

	for (i = 0; i < fc->SplinePts; i++) {
		fc->deriv[DimI][2] = fc->deriv[DimI][2] + fc->deriv[DimI][3];
		fc->deriv[DimI][1] = fc->deriv[DimI][1] + fc->deriv[DimI][2];
		fc->deriv[DimI][0] = fc->deriv[DimI][0] + fc->deriv[DimI][1];
		PTmp = fc->deriv[DimI][0] >> ShiftLen;
		fc->Plotlist[i][DimI] = fc->PStart[DimI] + PTmp;
	}
}

#define GetValue(fc, DimI, i) (fc)->Plotlist[i][DimI]

/* ------------------------------------------------------------------------ */
int main(int argc, char *argv[])
{

#define SCREEN_X 80
#define SCREEN_Y 80

	struct FredCurve fc;
	int DimI, i;

	unsigned char screen[SCREEN_Y][SCREEN_X] = {{0}};
	int spline[][DIMS] = {
		{SCREEN_X * 0.18, SCREEN_Y * 0.86, 0},
		{SCREEN_X * 0.44, SCREEN_Y * 0.96, 0},
		{SCREEN_X * 0.75, SCREEN_Y * 0.07, 0},
		{SCREEN_X * 0.86, SCREEN_Y * 0.75, 0},
		{SCREEN_X * 0.98,  SCREEN_Y * 0.61, 0},
		/* the way the code works,
		 * the last item needs to be included twice */
		{SCREEN_X * 0.98,  SCREEN_Y * 0.61, 0},
	};

	for (DimI = 0; DimI < DIMS; DimI++) {
		int gr = 1 << (sizeof(spline) / sizeof(*spline));
		InitSpline(&fc, DimI, spline[0][DimI], gr);
	}

	for (i = 0; i < sizeof(spline) / sizeof(*spline); i++) {
		for (DimI = 0; DimI < DIMS; DimI++) {
			Interpolate(&fc, DimI, spline[i][DimI]);
		}

		/* plot */
		{
			int k;
			for (k = 0; k < fc.SplinePts; k++) {
				int x = GetValue(&fc, 0, k);
				int y = GetValue(&fc, 1, k);
				screen[y][x] = 1;
			}

		}
	}

	for (i = 0; i < sizeof(spline) / sizeof(*spline); i++) {
		screen[spline[i][1]][spline[i][0]] = 2;
	}

	/* draw */
	{
		int x, y;
		const char fill[3] = {' ', '*', 'X'};
		printf("drawing: 'X'=control-point, '*'=interpolated-spline\n");
		for (y = 0; y < SCREEN_Y; y++) {
			for (x = 0; x < SCREEN_X; x++) {
				putchar(fill[screen[(SCREEN_Y - y) - 1][x]]);
			}
			putchar('\n');
		}

		printf("finished\n");
	}

	return 0;
}
